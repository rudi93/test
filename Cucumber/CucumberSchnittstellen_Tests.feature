# language: de
@BAB-Reha
Funktionalität: manuellen Anspruch anlegen

  @Niedrig
  @Anforderung(testfall = "manuellen Anspruch anlegen", id = "TF-00176", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=31627793-b9e9-a118-830e-7d0fc60ee28c")
  Szenario: Anlegen eines manuellen Anspruchs
    Angenommen Authentifizierung mit Standard-Benutzer "BenutzerSchreiben"
    Angenommen Schreibmodus für Werdegangseintrag "cd465fff-d27f-4146-88ee-f6f523d73d16"
    Wenn die Resource "manuellerabschnitt?werdegangseintragId=cd465fff-d27f-4146-88ee-f6f523d73d16" mit folgender Payload erzeugt wird
      """
      {
         "zeitraum": {
           "von": "2016-01-01",
           "bis": "2020-01-01"
         },
        "monatlicherAnspruch": {
          "currency": "EUR",
          "amount": "100"
        },
        "anteilKvPv": {
          "currency": "EUR",
          "amount": "50"
        },
      "regelmaessigkeit": "MONATLICH",
      "leistungsart": "BAB"
       }

      """
    Dann wird Http-Statuscode 200 erwartet
    Dann es ist folgender Datensatz in der Response enthalten
      | $._links                       | reg(.*)    |
      | $.id                           | reg(\\d+)  |
      | $.zeitraum.von                 | 2016-01-01 |
      | $.zeitraum.bis                 | 2020-01-01 |
      | $.monatlicherAnspruch.amount   |     100.00 |
      | $.monatlicherAnspruch.currency | EUR        |
      | $.anteilKvPv.amount            |      50.00 |
      | $.anteilKvPv.currency          | EUR        |
      | $.regelmaessigkeit             | MONATLICH  |
      | $.leistungsart                 | BAB        |

  @Niedrig
  @Anforderung(testfall = "manueller Anspruch bei Schreibgeschütztem Fall", id = "TF-00177", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=e7e33017-f4c2-0d37-e23a-4c2350e93fd3")
  Szenario: Anlegen eines manuellen Anspruchs bei einem Schreibgeschützen Fall
    Angenommen Authentifizierung mit Standard-Benutzer "BenutzerSchreiben"
    Angenommen Kein Schreibmodus für Werdegangseintrag "3b60526b-74b6-42c7-8e3f-9c038f37b734"
    Wenn die Resource "manuellerabschnitt?werdegangseintragId=3b60526b-74b6-42c7-8e3f-9c038f37b734" mit folgender Payload erzeugt wird
      """
      {
         "zeitraum": {
           "von": "2018-01-01",
           "bis": "2018-01-01"
         },
        "monatlicherAnspruch": {
          "currency": "EUR",
          "amount": "100"
        },
      "regelmaessigkeit": "MONATLICH",
      "leistungsart": "BAB"
       }

      """
    Dann wird Http-Statuscode 403 erwartet
    
    @Anforderung(testfall = "manueller neu angelegt - neuer name", id = "TF-00188", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=f5b43f84-ef1d-821f-50f0-997fc8668552")
  Szenario: Anlegen eines manuellen Anspruchs bei einem Schreibgeschützen Fall
    Angenommen Authentifizierung mit Standard-Benutzer "BenutzerSchreiben"
    Angenommen Kein Schreibmodus für Werdegangseintrag "3b60526b-74b6-42c7-8e3f-9c038f37b734"
    Wenn die Resource "manuellerabschnitt?werdegangseintragId=3b60526b-74b6-42c7-8e3f-9c038f37b734" mit folgender Payload erzeugt wird
      """
      {
         "zeitraum": {
           "von": "2018-01-01",
           "bis": "2018-01-01"
         },
        "monatlicherAnspruch": {
          "currency": "EUR",
          "amount": "100"
        },
      "regelmaessigkeit": "MONATLICH",
      "leistungsart": "BAB"
       }

      """
    Dann wird Http-Statuscode 403 erwartet

