package de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.manuel.entity;

import de.ba.operativ.leistung.bababgubg.plausibilisierung.Plausi;
import de.ba.operativ.leistung.bababgubg.plausibilisierung.ValidationExceptionMapper;
import de.ba.operativ.leistung.bababgubg.zeitoperationen.ZeitraumGeschlossen;
import de.ba.operativ.leistung.technischeplattform.fachfunktionen.datentypen.geldbetrag.Geldbetrag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

import static de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.entity.RegelmaessigkeitTyp.MONATLICH;
import static java.util.stream.Stream.of;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

public class ManuellerLeistungsanspruchValidationTest {

  @ParameterizedTest
  @MethodSource("validierungsDatenP2")
  public void testManellerLeistungsanspruchP2(String description, ManuellerLeistungsanspruch manuellerLeistungsanspruchAbschnitt, List<Plausi> expectedPlausis) {
    createAndValidateResponse(description, manuellerLeistungsanspruchAbschnitt, expectedPlausis);
  }

  /**
   * Prüft ob die Validierungen das richtige Verhalten zeigen und die Plausibilisierungen P2 mit dem richtigen Inhalt zurückgeliefert werden. Zusätzlich wird
   * überprüft, ob die richtige Anzahl an fehlgeschlagenen Validierungen zurückgeliefert wird
   */
  static Stream<Arguments> validierungsDatenP2() {
    return of(Arguments.of("Keine Plausi", //
            new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-01")), new Geldbetrag(123), MONATLICH, "BAB", null),//
            Collections.EMPTY_LIST),
          //Datumsfelder
            Arguments.of("Ein Zeitraum muss vorhanden sein", //
                    new ManuellerLeistungsanspruch(null, new Geldbetrag(123), MONATLICH, "BAB", null),//
                    Arrays.asList(new Plausi("F_P2_XX00280", "Bitte ein Datum angeben", "zeitraum"))),
            Arguments.of("Datum von und Datum bis sind Pflichtfelder", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(null, null), new Geldbetrag(123d), MONATLICH, "BAB", null),//
                    Arrays.asList(
                            new Plausi("F_P2_XX00280", "Bitte ein Datum angeben", "zeitraum.von"),
                            new Plausi("F_P2_XX00280", "Bitte ein Datum angeben", "zeitraum.bis")
                    )),
            Arguments.of("Datum von ist Pflichtfeld", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(null, LocalDate.parse("2020-01-01")), new Geldbetrag(123d), MONATLICH, "BAB", null),//
                    Arrays.asList(
                            new Plausi("F_P2_XX00280", "Bitte ein Datum angeben", "zeitraum.von")
                    )),
            Arguments.of("Datum bis ist Pflichtfeld", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), null), new Geldbetrag(123d), MONATLICH, "BAB", null),//
                    Arrays.asList(
                            new Plausi("F_P2_XX00280", "Bitte ein Datum angeben", "zeitraum.bis")
                    )),
            Arguments.of("Datum von ist vor dem bis Datum", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-02"), LocalDate.parse("2020-01-01")), new Geldbetrag(123d), MONATLICH, "BAB", null),//
                    Arrays.asList(new Plausi("F_P3_XX00297", "Das Bis-Datum darf nicht vor dem Von-Datum liegen", "zeitraum"))),
            Arguments.of("Daten von und bis müssen mindestens 1.1.1900 sein", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("1899-12-31"), LocalDate.parse("1899-12-31")), new Geldbetrag(123d), MONATLICH, "BAB", null),//
                    Arrays.asList(new Plausi("F_P2_XX00124", "Bitte ein Jahr ab 1900 eingeben", "zeitraum.von"),
                            new Plausi("F_P2_XX00124", "Bitte ein Jahr ab 1900 eingeben", "zeitraum.bis"))),

            //Betrag Anspruch
            Arguments.of("Anspruch ist Pflichtfeld", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), null, MONATLICH, "BAB", null),//
                    Arrays.asList(new Plausi("F_P2_XX00339", "Bitte einen Betrag angeben", "monatlicherAnspruch"))),
            Arguments.of("Anspruch ist Positiv", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(-123d), MONATLICH, "BAB", null),//
                    Arrays.asList(new Plausi("F_P2_XX00286", "Bitte eine positive Zahl eingeben", "monatlicherAnspruch"))),
            Arguments.of("Anspruch mit maximal 2 Nachkommastellen", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(123.0001d), MONATLICH, "BAB", null),//
                    Arrays.asList(new Plausi("F_P2_XX00118", "Bitte maximal 2 Nachkommastellen eingeben", "monatlicherAnspruch"))),
            Arguments.of("Anspruch ist maximal 99.999.999,99€", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(100000000), MONATLICH, "BAB", null),//
                    Arrays.asList(new Plausi("F_P2_XX00122", "Bitte maximal 99.999.999,99 € eingeben", "monatlicherAnspruch"))),
            
            //Leistungsart
            Arguments.of("Leistungsart ungültig", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(1), MONATLICH, "BBB", null),//
                    Arrays.asList(new Plausi("F_P2_XX00338", "Bitte einen gültigen Wert auswählen", "leistungsart"))),
            Arguments.of("Leistungsart Pflichtfeld", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(1), MONATLICH, null, null),//
                    Arrays.asList(new Plausi("F_P2_XX00309", "Bitte eine Auswahl treffen", "leistungsart"))),
            
            //Betrag Anteil KVPV
            Arguments.of("AnteilKVPV ist Positiv", //
                new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(123d), MONATLICH, "BAB", new Geldbetrag(-123d)),//
                Arrays.asList(new Plausi("F_P2_XX00286", "Bitte eine positive Zahl eingeben", "anteilKvPv"))),
            Arguments.of("AnteilKVPV mit maximal 2 Nachkommastellen", //
                new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(124d), MONATLICH, "BAB", new Geldbetrag(123.0001d)),//
                Arrays.asList(new Plausi("F_P2_XX00118", "Bitte maximal 2 Nachkommastellen eingeben", "anteilKvPv"))),
            Arguments.of("AnteilKVPV ist maximal 99.999.999,99€", //
                new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(100000001), MONATLICH, "BAB", new Geldbetrag(100000000)),//
                Arrays.asList(new Plausi("F_P2_XX00122", "Bitte maximal 99.999.999,99 € eingeben", "monatlicherAnspruch"),
                              new Plausi("F_P2_XX00122", "Bitte maximal 99.999.999,99 € eingeben", "anteilKvPv"))),
            Arguments.of("Anteil KV PV höher als Manueller Anspruch", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(1), MONATLICH, "BAB", new Geldbetrag(1.01d)),//
                    Arrays.asList(new Plausi("F_P3_XX00126", "Der Anteil an freiwillig gesetzlicher/privater KV/PV darf nicht höher sein als der manuelle Anspruch", ""))),
            
            //Regelmässigkeit
            Arguments.of("Regelmässigkeit ist Pflichtfeld", //
                    new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.parse("2020-01-01"), LocalDate.parse("2020-01-02")), new Geldbetrag(1), null, "BAB", null),//
                    Arrays.asList(new Plausi("F_P2_XX00309", "Bitte eine Auswahl treffen", "regelmaessigkeit")))
    );
  }


  private <T> void createAndValidateResponse(String description, T manuelleLeistung, List<Plausi> expectedPlausis) {
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    Set<ConstraintViolation<T>> constraintViolations = validator.validate(manuelleLeistung);

    ValidationExceptionMapper mapper = new ValidationExceptionMapper();
    Response response = mapper.toResponse(new ConstraintViolationException(constraintViolations));
    @SuppressWarnings("unchecked")
    Map<String, Map<String, List<Plausi>>> responseEntity = (Map<String, Map<String, List<Plausi>>>) response.getEntity();

    if (!expectedPlausis.isEmpty()) {
      assertThat("Response Entity muss problems enthalten.", responseEntity.get("problems"), is(notNullValue()));
      assertThat("problems muss messages", responseEntity.get("problems").get("messages"), is(notNullValue()));
    }

    List<Plausi> plausis = responseEntity.get("problems").get("messages");

    expectedPlausis.sort(Comparator.comparing(Plausi::getMeldungsId)
            .thenComparing(Plausi::getFeld));
    plausis.sort(Comparator.comparing(Plausi::getMeldungsId)
            .thenComparing(Plausi::getFeld));

    assertThat("Anzahl der Erwarteten Plausis stimmt nicht mit der ermittelten überein.", plausis.size(), is(expectedPlausis.size()));
    assertAll(description, () -> {
      for (int i = 0; i < plausis.size(); i++) {
        Plausi pExpected = expectedPlausis.get(i);
        Plausi pActual = plausis.get(i);
        assertAll(() -> assertThat(pActual.getMeldungsId(), is(pExpected.getMeldungsId())),
                () -> assertThat(pActual.getMeldungstext(), is(pExpected.getMeldungstext())), () -> assertThat(pActual.getFeld(), is(pExpected.getFeld())));
      }
    });
  }

}
