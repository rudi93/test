package de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.manuel.control;

import de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.entity.RegelmaessigkeitTyp;
import de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.manuel.basisdaten.entity.LeistungsartFachIds;
import de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.manuel.entity.ManuellerLeistungsanspruch;
import de.ba.operativ.leistung.bababgubg.zeitoperationen.Zeitraum;
import de.ba.operativ.leistung.bababgubg.zeitoperationen.ZeitraumGeschlossen;
import de.ba.operativ.leistung.technischeplattform.fachfunktionen.datentypen.geldbetrag.Geldbetrag;
import de.ba.operativ.leistung.technischeplattform.fachfunktionen.falldaten.EntityNichtVorhandenException;
import de.ba.operativ.leistung.technischeplattform.fachfunktionen.falldaten.FalldatenService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.NotFoundException;
import java.time.LocalDate;
import java.util.*;

import static de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.entity.RegelmaessigkeitTyp.MONATLICH;
import static de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.manuel.basisdaten.entity.LeistungsartFachIds.BAB;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * Testet die Methoden von ManuellerAnspruchController
 */
@ExtendWith(MockitoExtension.class)
class ManuellerAnspruchControllerTest {

  @Mock
  FalldatenService falldatenService;

  @InjectMocks
  ManuellerAnspruchController cut;

  /**
   * Prüft ob die Abfrage der Ansprüche richtig an den Falldatenservice weitergegeben wird
   */
  @Test
  void testlisteErfassteAnspruecheAuf() {
    List<ManuellerLeistungsanspruch> lst = new LinkedList<>();
    LocalDate now = LocalDate.now();
    Random rnd = new Random();
    for (int i = 0; i < 10; i++) {
      LocalDate von = now.plusDays(rnd.nextInt(100));
      LocalDate bis = von.plusDays(5);
      lst.add(new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(von, bis), null, MONATLICH, BAB.name(), null));
    }

    when(falldatenService.leseNeustandZuFall(eq(ManuellerLeistungsanspruch.class), anyString())).thenReturn(lst);

    List<ManuellerLeistungsanspruch> result = cut.listeErfassteAnspruecheAuf("fallId");

    verify(falldatenService, times(1)).leseNeustandZuFall(any(), anyString());

    assertNotNull(result);
    Iterator<ManuellerLeistungsanspruch> it = result.iterator();
    ManuellerLeistungsanspruch before = it.next();
    assertNotNull(before);
    while (it.hasNext()) {
      ManuellerLeistungsanspruch mla = it.next();
      assertNotNull(mla);

      assertTrue(before.getZeitraum().getVon().compareTo(mla.getZeitraum().getVon()) >= 0, "Sortierung nach Von-Datum nicht mehr vorhanden");
      before = mla;
    }
  }

  /**
   * Prüft ob ein erfasster Anspruch richtig an den Falldatenservice zur Speicherung weitergegeben wird
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  @Test
  void testGetErfasstenAnspruch() {
    long now = System.currentTimeMillis();

    cut.getErfasstenAnspruch("fallId", Long.valueOf(now));

    ArgumentCaptor<Class> clsCaptor = ArgumentCaptor.forClass(Class.class);
    ArgumentCaptor<String> strCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Long> longCaptor = ArgumentCaptor.forClass(Long.class);

    verify(falldatenService, times(1)).lese(clsCaptor.capture(), strCaptor.capture(), longCaptor.capture());

    assertEquals(ManuellerLeistungsanspruch.class, clsCaptor.getValue());
    assertEquals("fallId", strCaptor.getValue());
    assertEquals(now, longCaptor.getValue().longValue());
  }

  /**
   * Prüft ob eine Löschung an den Falldatenservice weiter delegiert wird
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  @Test
  void testLoescheErfasstenAnspruch() {
    long now = System.currentTimeMillis();

    cut.loescheErfasstenAnspruch("fallId", Long.valueOf(now));

    ArgumentCaptor<Class> clsCaptor = ArgumentCaptor.forClass(Class.class);
    ArgumentCaptor<String> strCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Long> longCaptor = ArgumentCaptor.forClass(Long.class);
    verify(falldatenService, times(1)).loesche(clsCaptor.capture(), strCaptor.capture(), longCaptor.capture());

    assertEquals(ManuellerLeistungsanspruch.class, clsCaptor.getValue());
    assertEquals("fallId", strCaptor.getValue());
    assertEquals(now, longCaptor.getValue().longValue());
  }

  /**
   * Prüft ob ein neuer erfasster Leistungsanspruch richtig an den Falldatenservice weitergegeben wird
   */
  @Test
  void testErfasseAnspruch() {
    ManuellerLeistungsanspruch mla = new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.MIN, LocalDate.MAX), new Geldbetrag(2.345D), MONATLICH, BAB.name(), null);
    assertNull(mla.getId());

    Long id = cut.erfasseAnspruch("fallId", mla);

    assertNull(id, "Sollte keine Id liefern da nur Mock angesprochen wird");

    ArgumentCaptor<ManuellerLeistungsanspruch> mlaCaptor = ArgumentCaptor.forClass(ManuellerLeistungsanspruch.class);
    ArgumentCaptor<String> strCaptor = ArgumentCaptor.forClass(String.class);
    verify(falldatenService, times(1)).erfasse(mlaCaptor.capture(), strCaptor.capture());

    assertEquals("fallId", strCaptor.getValue());
    assertEquals(mla, mlaCaptor.getValue());
  }

  /**
   * Prüft ob bei einer Änderung der Falldatenservice richtig aufgerufen wird
   */
  @Test
  void testMergeErfasstenAnspruch() {
    ManuellerLeistungsanspruch mla = new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.MIN, LocalDate.MIN), new Geldbetrag(2.345D), MONATLICH, BAB.name(), null);
    mla.setId(System.currentTimeMillis());
    assertNotNull(mla.getId());

    ManuellerLeistungsanspruch result = cut.mergeErfasstenAnspruch("fallId", mla.getId(), mla);

    assertNull(result, "Nur Mock, daher kein Resultat");

    ArgumentCaptor<ManuellerLeistungsanspruch> mlaCaptor = ArgumentCaptor.forClass(ManuellerLeistungsanspruch.class);
    ArgumentCaptor<String> strCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Long> longCaptor = ArgumentCaptor.forClass(Long.class);
    verify(falldatenService, times(1)).mergeAenderung(mlaCaptor.capture(), strCaptor.capture(), longCaptor.capture());

    assertEquals(mla, mlaCaptor.getValue());
    assertEquals("fallId", strCaptor.getValue());
    assertEquals(mla.getId().longValue(), longCaptor.getValue().longValue());
  }

  @Test
  void testMergeErfasstenAnspruchhNichtVorhanden() {
    when(falldatenService.mergeAenderung(any(), any(), any())).thenThrow(new EntityNichtVorhandenException(String.class, "fallId", System.currentTimeMillis()));

    assertThrows(NotFoundException.class, () -> {
      this.cut.mergeErfasstenAnspruch("fallId", 123L, mock(ManuellerLeistungsanspruch.class));
    });
  }

  @Test
  void testLoescheErfasstenAnspruchNichtVorhanden() {
    doThrow(new EntityNichtVorhandenException(String.class, "fallId", System.currentTimeMillis())).when(falldatenService).loesche(any(), any(), any());

    assertThrows(NotFoundException.class, () -> {
      this.cut.loescheErfasstenAnspruch("fallId", 123L);
    });
  }

  @Test
  void listeErfassteAnspruecheAufTest() {
    String fallId = "abc";
    Zeitraum zr = new ZeitraumGeschlossen(LocalDate.of(2020, 02, 01), LocalDate.of(2020, 11, 30));

    ManuellerLeistungsanspruch m1 = new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 12, 31))
            , new Geldbetrag(-123), MONATLICH, BAB.name(), null);
    ManuellerLeistungsanspruch m2 = new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 31))
            , new Geldbetrag(-321), MONATLICH, BAB.name(), null);
    ManuellerLeistungsanspruch m3 = new ManuellerLeistungsanspruch(new ZeitraumGeschlossen(LocalDate.of(2020, 12, 01), LocalDate.of(2020, 12, 31))
            , new Geldbetrag(-666), MONATLICH, BAB.name(), null);

    List<ManuellerLeistungsanspruch> manLstg = new ArrayList<>(Arrays.asList(m1, m2, m3));

    when(falldatenService.leseNeustandZuFall(eq(ManuellerLeistungsanspruch.class), eq(fallId))).thenReturn(manLstg);
    List<ManuellerLeistungsanspruch> result = cut.listeErfassteAnspruecheAuf(fallId, zr);
    assertNotNull(result);
    assertEquals(result.size(), 1);
    assertEquals(result.get(0).getMonatlicherAnspruch(), new Geldbetrag(-123));
  }

}
