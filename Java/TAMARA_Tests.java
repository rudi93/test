package de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.automatisierung.leistungsanspruch.manuell;

import de.ba.operativ.leistung.bababgubg.tamarautils.ScreenshotRule;
import de.ba.operativ.leistung.bababgubg.tamarautils.annotations.NeuerTestfall;
import de.ba.operativ.leistung.bababgubg.tamarautils.annotations.Systemtest;
import de.ba.operativ.leistung.bababgubg.tamarautils.annotations.TestTags;
import de.ba.swbib.starter.SW;
import de.ba.swbib.starter.annotation.Objektliste;
import de.ba.swbib.starter.junit.Testfall;
import org.junit.Rule;
import org.junit.Test;

@Objektliste({ "BAB_REHA", "BAB_REHA_manuellerLeistungsanspruch_Uebersicht", "BAB_REHA_manuellerLeistungsanspruch_Hinzufuegen", "BAB_REHA_manuellerLeistungsanspruch_Objektsammlung" })
public class ManuellenAnspruchAnlegen extends Testfall {

  @Rule
  public ScreenshotRule screenshotRule = new ScreenshotRule();

  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8187" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Anlegen() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");
    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");
    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "1Anlegen");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.PrüfeWert("TBL_manuellerLeistungsanspruch#TABELLE#ZEILE:1",
        "BAB<TRENNER><ZEITPUNKT=JETZT(TT.MM.JJJJ)><TRENNER><ZEITPUNKT=JETZT+1T(TT.MM.JJJJ)><TRENNER>Monatlich<TRENNER>300,00 €<TRENNER><TRENNER>Öffnen");

  }

  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8187" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Felder_initial_Leer() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");
    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.PrüfeWert("TXT_manuellerAnspruch_ZeitraumVon", "<LEER>");
    SW.PrüfeWert("TXT_manuellerAnspruch_ZeitraumBis", "<LEER>");
    SW.PrüfeWert("TXT_manuellerAnspruch_Betrag", "<LEER>");
  }

  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-5660", "BABABGUBG-1277", "BABABGUBG-8187" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Speichern_Fokus_Fehlermeldung() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "VonLeer");
    SW.PrüfeWert("TXT_manuellerAnspruch_ZeitraumVon#FOKUS", "JA");
    SW.PrüfeWert("TXT_manuellerAnspruch_FehlerZeitraumVon#VORHANDEN", "JA");

    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "BisLeer");
    SW.PrüfeWert("TXT_manuellerAnspruch_ZeitraumBis#FOKUS", "JA");
    SW.PrüfeWert("TXT_manuellerAnspruch_FehlerZeitraumBis#VORHANDEN", "JA");

    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "AnspruchLeer");
    SW.PrüfeWert("TXT_manuellerAnspruch_Betrag#FOKUS", "JA");
    SW.PrüfeWert("TXT_Fehlermeldung_Betrag#VORHANDEN", "JA");

  }

  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8177" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Speichern_nach_Fehler() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");
    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerAnspruch_Speichern");
    SW.PrüfeWert("TXT_manuellerAnspruch_FehlerZeitraumVon#VORHANDEN", "JA");
    SW.PrüfeWert("TXT_manuellerAnspruch_FehlerZeitraumBis#VORHANDEN", "JA");
    SW.PrüfeWert("TXT_Fehlermeldung_Betrag#VORHANDEN", "JA");

    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "1Anlegen");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.PrüfeWert("TBL_manuellerLeistungsanspruch#TABELLE#ZEILE:1",
        "BAB<TRENNER><ZEITPUNKT=JETZT(TT.MM.JJJJ)><TRENNER><ZEITPUNKT=JETZT+1T(TT.MM.JJJJ)><TRENNER>Monatlich<TRENNER>300,00 €<TRENNER><TRENNER>Öffnen");
  }

  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8177" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Hinweistext_Schreibschutz() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch/neu");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesen");
    SW.StarteAnwendung("BAB-REHA");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.PrüfeWert("TXT_Hinweismeldung#VORHANDEN", "JA");
  }

  /**
   * BARRIEREFREIHEITS TESTS
   */

  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8177" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Hinzufuegen_Barrierefreiheit() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.PrüfeWert("manuellerLeistungsanspruch_Hinzufuegen#BARRIEREFREI", "STANDARD");
  }

}
