package de.ba.operativ.leistung.bababgubg.leistungsanspruchbabm.automatisierung.leistungsanspruch.manuell;

import de.ba.operativ.leistung.bababgubg.tamarautils.ScreenshotRule;
import de.ba.operativ.leistung.bababgubg.tamarautils.annotations.NeuerTestfall;
import de.ba.operativ.leistung.bababgubg.tamarautils.annotations.Systemtest;
import de.ba.operativ.leistung.bababgubg.tamarautils.annotations.TestTags;
import de.ba.swbib.starter.SW;
import de.ba.swbib.starter.annotation.Objektliste;
import de.ba.swbib.starter.junit.Testfall;
import org.junit.Rule;
import org.junit.Test;

@Objektliste({ "BAB_REHA", "BAB_REHA_manuellerLeistungsanspruch_Uebersicht", "BAB_REHA_manuellerLeistungsanspruch_Hinzufuegen", "BAB_REHA_manuellerLeistungsanspruch_Objektsammlung" })
public class ManuellenAnspruchAnlegen extends Testfall {

  @Rule
  public ScreenshotRule screenshotRule = new ScreenshotRule();

  @Anforderung(testfall = "GELÖSCHT - BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Anlegen", id = "TF-00210", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=57dcc927-084a-e20a-7d7e-124618f51249")
  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8187" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Anlegen() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");
    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");
    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "1Anlegen");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.PrüfeWert("TBL_manuellerLeistungsanspruch#TABELLE#ZEILE:1",
        "BAB<TRENNER><ZEITPUNKT=JETZT(TT.MM.JJJJ)><TRENNER><ZEITPUNKT=JETZT+1T(TT.MM.JJJJ)><TRENNER>Monatlich<TRENNER>300,00 €<TRENNER><TRENNER>Öffnen");

  }

  @Anforderung(testfall = "GELÖSCHT - BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Felder_initial_Leer", id = "TF-00211", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=f17fc079-52dc-a87f-2cf7-00ec4223b72d")
  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8187" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Felder_initial_Leer() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");
    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.PrüfeWert("TXT_manuellerAnspruch_ZeitraumVon", "<LEER>");
    SW.PrüfeWert("TXT_manuellerAnspruch_ZeitraumBis", "<LEER>");
    SW.PrüfeWert("TXT_manuellerAnspruch_Betrag", "<LEER>");
  }

  @Anforderung(testfall = "GELÖSCHT - BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Speichern_Fokus_Fehlermeldung", id = "TF-00212", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=baa24c84-af00-be1c-411d-3f8e16188b80")
  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-5660", "BABABGUBG-1277", "BABABGUBG-8187" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Speichern_Fokus_Fehlermeldung() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "VonLeer");
    SW.PrüfeWert("TXT_manuellerAnspruch_ZeitraumVon#FOKUS", "JA");
    SW.PrüfeWert("TXT_manuellerAnspruch_FehlerZeitraumVon#VORHANDEN", "JA");

    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "BisLeer");
    SW.PrüfeWert("TXT_manuellerAnspruch_ZeitraumBis#FOKUS", "JA");
    SW.PrüfeWert("TXT_manuellerAnspruch_FehlerZeitraumBis#VORHANDEN", "JA");

    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "AnspruchLeer");
    SW.PrüfeWert("TXT_manuellerAnspruch_Betrag#FOKUS", "JA");
    SW.PrüfeWert("TXT_Fehlermeldung_Betrag#VORHANDEN", "JA");

  }

  @Anforderung(testfall = "GELÖSCHT - BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Speichern_nach_Fehler", id = "TF-00213", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=71c76fb8-8302-ad9a-48bb-57242e9e3823")
  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8177" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Speichern_nach_Fehler() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");
    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerAnspruch_Speichern");
    SW.PrüfeWert("TXT_manuellerAnspruch_FehlerZeitraumVon#VORHANDEN", "JA");
    SW.PrüfeWert("TXT_manuellerAnspruch_FehlerZeitraumBis#VORHANDEN", "JA");
    SW.PrüfeWert("TXT_Fehlermeldung_Betrag#VORHANDEN", "JA");

    SW.WähleSequenz("manuellerAnspruch.manuellenAnspruchAnlegen", "SEQ_manuellerAnspruch_Varianten.xlsx", "1Anlegen");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.PrüfeWert("TBL_manuellerLeistungsanspruch#TABELLE#ZEILE:1",
        "BAB<TRENNER><ZEITPUNKT=JETZT(TT.MM.JJJJ)><TRENNER><ZEITPUNKT=JETZT+1T(TT.MM.JJJJ)><TRENNER>Monatlich<TRENNER>300,00 €<TRENNER><TRENNER>Öffnen");
  }

  @Anforderung(testfall = "GELÖSCHT - BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Hinweistext_Schreibschutz", id = "TF-00214", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=a5ebb6d4-f25c-d599-3167-9ac47a28f3c9")
  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8177" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Hinweistext_Schreibschutz() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch/neu");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesen");
    SW.StarteAnwendung("BAB-REHA");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.PrüfeWert("TXT_Hinweismeldung#VORHANDEN", "JA");
  }

  /**
   * BARRIEREFREIHEITS TESTS
   */
  @Anforderung(testfall = "GELÖSCHT - BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Hinzufuegen_Barrierefreiheit", id = "TF-00215", innolink = "inno:///BA_BAB_REHA_20200403/INOX/BAB_REHA?uuid=8cb83f7d-01dc-5a61-0ba9-19d36bab74c3")
  @Test
  @Systemtest
  @TestTags(tags = { "BABABGUBG-8177" })
  public void BABREHA_Leistungsanspruchbabm_GUI_manuellenLeistungsanspruch_Hinzufuegen_Barrierefreiheit() {
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Leistungsanspruch", "URL", "WERDEGANGID", "KONTEXT",
        "manuellerleistungsanspruch");
    SW.ErzeugeStartzustand("startzustände.Leistungsanspruch.Authentifizierung", "BENUTZER", "BenutzerLesenSchreiben");
    SW.StarteAnwendung("BAB-REHA");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Uebersicht");
    SW.WähleAus("SCHALTFLÄCHE", "BTN_manuellerLeistungsanspruch_Neu");

    SW.WähleAus("FENSTER", "manuellerLeistungsanspruch_Hinzufuegen");
    SW.PrüfeWert("manuellerLeistungsanspruch_Hinzufuegen#BARRIEREFREI", "STANDARD");
  }

}
