import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Store, StoreModule } from '@ngrx/store';
import {
  FipTranslateModule,
  FormConnectorModule,
  GeldbetragModule,
  reducers,
  State,
  Zeitraum,
  ZeitraumModule
} from '@bababgubg/fip';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators
} from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { LeistungsanspruchErfassenComponent } from './leistungsanspruch-erfassen.component';
import {
  initialManuellerLeistungsanspruchBasisdaten,
  initialManuellerLeistungsanspruchListe,
  initialSelectedManuellerLeistungsanspruch,
  manuellerleistungsanspruchReducer,
  ManuellerLeistungsanspruchState
} from '../state/manuellerleistungsanspruch.reducer';
import { ManuellerLeistungsanspruch } from '../model/manuellerLeistungsanspruch';
import { ManuellerLeistungsanspruchListe } from '../model/manuellerLeistungsanspruchListe';
import {
  ChangeAction,
  ChangeAnteilKvPv,
  ChangeZeitraum,
  CreateManuellerLeistungsanspruch,
  ManuellerLeistunganspruchActionTypes,
  UpdateManuellerLeistungsanspruchEintrag
} from '../state/manuellerleistungsanspruch.actions';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material';

const manuellerLeistungsanspruch: ManuellerLeistungsanspruch = {
  id: undefined,
  zeitraum: {
    von: '05-10-2018',
    bis: '10-10-2018'
  },
  monatlicherAnspruch: {
    amount: '500',
    currency: 'EUR'
  },
  anteilKvPv: {
    amount: '100',
    currency: 'EUR'
  },
  _links: {['manuellerabschnitt:delete']: {href: 'PlatzhalterURL'}}
};

const manuellerLeistungsanspruchOhneId: ManuellerLeistungsanspruch = {
  id: undefined,
  zeitraum: {
    von: '05-10-2018',
    bis: '10-10-2018'
  },
  monatlicherAnspruch: {
    amount: '500',
    currency: 'EUR'
  },
  anteilKvPv: {
    amount: '100',
    currency: 'EUR'
  },
  _links: {['manuellerabschnitt:delete']: {href: 'PlatzhalterURL'}}
};

const manuellerLeistungsanspruchMitDeleteLinkMock: ManuellerLeistungsanspruch = {
  id: 2,
  zeitraum: {
    von: '05-10-2018',
    bis: '10-10-2018'
  },
  monatlicherAnspruch: {
    amount: '500',
    currency: 'EUR'
  },
  anteilKvPv: {
    amount: '100',
    currency: 'EUR'
  },
  _links: {['manuellerabschnitt:delete']: {href: 'PlatzhalterURL'}}
};

const manuellerLeistungsanspruchMitUpdateLinkMock: ManuellerLeistungsanspruch = {
  id: 2,
  zeitraum: {
    von: '05-10-2018',
    bis: '10-10-2018'
  },
  monatlicherAnspruch: {
    amount: '500',
    currency: 'EUR'
  },
  anteilKvPv: {
    amount: '100',
    currency: 'EUR'
  },
  _links: {['manuellerabschnitt:update']: {href: 'PlatzhalterURL'}}
};

const manuellerLeistungsanspruchListeMitCreateLinkMock: ManuellerLeistungsanspruchListe = {
  _links: {['manuellerabschnitt:create']: {href: 'PlatzhalterURL'}},
  manuellerleistungsanspruch: [{...manuellerLeistungsanspruchMitUpdateLinkMock}]
};

const manuellerLeistungsanspruchListeMitDeleteLinkMock: ManuellerLeistungsanspruchListe = {
  _links: {['manuellerabschnitt:delete']: {href: 'PlatzhalterURL'}},
  manuellerleistungsanspruch: [{...manuellerLeistungsanspruchMitUpdateLinkMock}]
};

const manuellerLeistungsanspruchState: ManuellerLeistungsanspruchState = {
  manuellerLeistungsanspruchListe: initialManuellerLeistungsanspruchListe,
  selectedManuellerLeistungsanspruch: initialSelectedManuellerLeistungsanspruch,
  selectedManuellerLeistungsanspruchErrors: [],
  showDeleteDialog: false,
  isError: false,
  isLoading: false,
  isSaving: false,
  errorMessage: '',
  basisdaten: initialManuellerLeistungsanspruchBasisdaten
};

const manuellerLeistungsanspruchStateCreate: ManuellerLeistungsanspruchState = {
  ...manuellerLeistungsanspruchState
};

describe('Manueller Leistungsanspruch ErfassenComponent', () => {
  let component: LeistungsanspruchErfassenComponent;
  let fixture: ComponentFixture<LeistungsanspruchErfassenComponent>;
  let store$: Store<State>;
  let router: Router;
  let activatedRoute: ActivatedRoute;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(
          {
            ...reducers,
            leistungsanspruchReducer: manuellerleistungsanspruchReducer
          }),
        BrowserAnimationsModule,
        HttpClientTestingModule,
        MatCardModule,
        MatTableModule,
        MatSelectModule,
        MatOptionModule,
        MatInputModule,
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        ReactiveFormsModule,
        RouterTestingModule,
        FormConnectorModule,
        FipTranslateModule,
        GeldbetragModule,
        ZeitraumModule,
        TranslateModule.forRoot(),
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of([{id: 1}])
          }
        }
      ],
      declarations: [LeistungsanspruchErfassenComponent]
    });

    fixture = TestBed.createComponent(LeistungsanspruchErfassenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store$ = TestBed.get(Store);
    activatedRoute = TestBed.get(ActivatedRoute);
    router = TestBed.get(Router);
  });

  it('kann erstellt werden', () => {
    expect(fixture).toBeDefined();
  });

  it('soll eine Instanz erstellen', () => {
    expect(component).toBeTruthy();
  });

  it('schickt eine ChangeMonatlicherAnspruch Action ab, wenn onChangeMonatlicherAnspruch aufgerufen', () => {
    spyOn(store$, 'dispatch');
    fixture.detectChanges();

    component.onChangeMonatlicherAnspruch('manuellerleistungsanspruch');

    expect(store$.dispatch).toHaveBeenCalled();
  });

  describe('Formular abschicken und speichern', () => {
    it('löst das Speichern beim Button click aus, wenn das Formular valide ist', () => {
      component.anspruchForm = formBuilder.group({
        zeitraum: '',
        leistungsanspruch: '42'
      });

      spyOn(component, 'speichern');
      component.onButtonClick(manuellerLeistungsanspruchMitDeleteLinkMock);
      fixture.detectChanges();

      expect(component.speichern).toHaveBeenCalled();
    });

    it('löst nicht das Speichern beim Button click aus, wenn das Formular nicht valide ist', () => {
      spyOn(component, 'speichern');
      component.onButtonClick(manuellerLeistungsanspruchMitDeleteLinkMock);
      fixture.detectChanges();

      expect(component.speichern).not.toHaveBeenCalled();
    });

    // tslint:disable-next-line:max-line-length
    it('schickt UpdateManuellerleistungsanspruch-Action ab, wenn speichern aufgerufen und (manuellerleistungsanspruch.id !== undefined) ist', () => {
      spyOn(store$, 'dispatch');
      fixture.detectChanges();

      component.speichern(manuellerLeistungsanspruchMitUpdateLinkMock);

      expect(store$.dispatch).toHaveBeenCalledWith(new UpdateManuellerLeistungsanspruchEintrag({
        manuellerleistungsanspruch: manuellerLeistungsanspruchMitUpdateLinkMock,
        activatedRoute
      }));
      expect(store$.dispatch).toHaveBeenCalled();
    });

    it('schickt CreateManuellerLeistungsanspruch-Action ab, wenn speichern aufgerufen und (unterbringung.id === undefined) ist', () => {
      spyOn(store$, 'dispatch');
      fixture.detectChanges();

      component.speichern(manuellerLeistungsanspruchListeMitCreateLinkMock);

      // tslint:disable-next-line:max-line-length
      expect(store$.dispatch).toHaveBeenCalledWith(new CreateManuellerLeistungsanspruch({
        manuellerleistungsanspruch: manuellerLeistungsanspruchListeMitCreateLinkMock,
        activatedRoute
      }));
      expect(store$.dispatch).toHaveBeenCalled();
    });

  });

  it('dispatcht eine ChangeAnteilKvPv action, wenn onChangeAnteilKvPv aufegerufen wird', () => {
    const anteilKvPv = '100.00';

    spyOn(store$, 'dispatch');
    fixture.detectChanges();

    component.onChangeAnteilKvPv(anteilKvPv);

    expect(store$.dispatch).toHaveBeenCalledWith(new ChangeAnteilKvPv(anteilKvPv));
  });

  it('dispatcht eine ChangeZeitraum Aktion, wenn onZeitraumChange aufgerufen wird', () => {
    const zeitraum: Zeitraum = {
      von: '1970-01-01',
      bis: '1970-01-01'
    };
    spyOn(store$, 'dispatch');
    fixture.detectChanges();

    component.onZeitraumChange(zeitraum);

    expect(store$.dispatch).toHaveBeenCalledWith(new ChangeZeitraum(zeitraum));
  });

  it('dispatcht eine ChangeAction, wenn onRegelmaessigkeitSelect aufgerufen wird', () => {
    spyOn(store$, 'dispatch');
    fixture.detectChanges();

    component.onRegelmaessigkeitSelect();

    expect(store$.dispatch).toHaveBeenCalledWith(
      new ChangeAction('', ManuellerLeistunganspruchActionTypes.ChangeRegelmaessigkeit));
  });

  it('dispatcht eine ChangeAction, wenn onLeistungsartSelect aufegerufen wird', () => {
    spyOn(store$, 'dispatch');
    fixture.detectChanges();

    component.onLeistungsartSelect();

    expect(store$.dispatch).toHaveBeenCalledWith(
      new ChangeAction('', ManuellerLeistunganspruchActionTypes.ChangeLeistungsart));
  });

  it('prüft ob erzeugen erlaubt ist, in dem getestet wird ob der createlink vorhanden ist', () => {
    expect(component.istErzeugenErlaubt(manuellerLeistungsanspruchListeMitCreateLinkMock))
      .toEqual(true);
  });

  it('prüft ob bearbeiten erlaubt ist, in dem getestet wird ob der updatelink vorhanden ist', () => {
    expect(component.istBearbeitenErlaubt(manuellerLeistungsanspruchMitUpdateLinkMock))
      .toEqual(true);
  });

  it('prüft ob HinweisMeldung ist, in dem getestet wird ob der create-Link im State vorhanden ist', () => {
    expect(component.istHinweisMeldung(manuellerLeistungsanspruchStateCreate))
      .toEqual(true);
  });

  // Tests fuer 'fehlermeldungenAusBackend'
  [
    {
      testfall: 'Fehlermeldung mit allen Feldern',
      validationErrors: [{feld: 'test', meldungsId: 'F_P2_XXX1234', meldungstext: 'abc abc abc'}],
      expectedMeldungen: ['F_P2_XXX1234: abc abc abc (Test)']
    },
    {
      testfall: 'Fehlermeldung ohne feld',
      validationErrors: [{meldungsId: 'F_P2_XXX1234', meldungstext: 'abc abc abc'}],
      expectedMeldungen: ['F_P2_XXX1234: abc abc abc']
    },
    {
      testfall: 'Fehlermeldung ohne Meldungsid ',
      validationErrors: [{feld: 'test', meldungstext: 'abc abc abc'}],
      expectedMeldungen: ['abc abc abc (Test)']
    },
    {
      testfall: 'Fehlermeldung ohne Meldungstext',
      validationErrors: [{feld: 'test', meldungsId: 'F_P2_XXX1234'}],
      expectedMeldungen: ['F_P2_XXX1234 (Test)']
    },
    {
      testfall: 'Fehlermeldung mit camel case Feldid',
      validationErrors: [{feld: 'testTestTest', meldungsId: 'F_P2_XXX1234'}],
      expectedMeldungen: ['F_P2_XXX1234 (Test Test Test)']
    },
    {
      testfall: 'Fehlermeldung mit zusammengesetzten Id',
      validationErrors: [{feld: 'TestTest.testTestTest', meldungsId: 'F_P2_XXX1234'}],
      expectedMeldungen: ['F_P2_XXX1234 (Test Test Test)']
    },
    {
      testfall: 'Fehlermeldung mit leerem feld ',
      validationErrors: [{feld: '', meldungsId: 'F_P2_XXX1234', meldungstext: 'abc abc abc'}],
      expectedMeldungen: ['F_P2_XXX1234: abc abc abc']
    }
  ].forEach((testfall) => {
      it(`disabled das monatlicher Anspruch Control, wenn '${testfall.testfall}' ausgewählt wurde`, () => {

        expect(component.fehlermeldungenAusBackend(testfall.validationErrors)).toEqual(testfall.expectedMeldungen);
      });
    }
  );

  // Getter fuer FormControl
  [
    {
      formControl: 'monatlicherAnspruchControl',
      getter: 'monatlicherAnspruch'
    },
    {
      formControl: 'zeitraumControl',
      getter: 'zeitraum'
    }
  ].forEach((testfall) => {
    it(`getter methode für ${testfall.getter}`, () => {

      // @ts-ignore
      expect(fixture.componentInstance[testfall.formControl]).toEqual(fixture.componentInstance.anspruchForm.controls[testfall.getter]);
    });
  });

  // Tests 'Subscriptions'
  it('soll die Subscription auf Load loadLeistungsanspruchEintrag im ngOnDestroy löschen', () => {
    fixture.destroy();

    expect(component.loadLeistungsanspruchEintragSubscription).not.toBeDefined();
  });

  it('wenn loadLeistungsanspruchEintragSubscription undefined ist, bleibt es im ngOnDestroy auch undefined', () => {
    component.loadLeistungsanspruchEintragSubscription = undefined;
    fixture.destroy();

    expect(component.loadLeistungsanspruchEintragSubscription).not.toBeDefined();
  });

  it('soll die Subscription auf Load loadLeistungsanspruchFormSubscription im ngOnDestroy löschen', () => {
    fixture.destroy();

    expect(component.loadLeistungsanspruchFormSubscription).not.toBeDefined();
  });

  it('wenn loadLeistungsanspruchFormSubscription undefined ist, bleibt es im ngOnDestroy auch undefined', () => {
    component.loadLeistungsanspruchFormSubscription = undefined;
    fixture.destroy();

    expect(component.loadLeistungsanspruchFormSubscription).not.toBeDefined();
  });

  // onZurueckButtonClick
  it('ruft router navigate auf, wenn onZurueckButtonClick aufgerufen wird', () => {
    const spy = spyOn(router, 'navigate');
    fixture.detectChanges();

    component.onZurueckButtonClick();

    expect(spy).toHaveBeenCalled();
  });

  // onAbbrechen
  it('ruft router navigate auf, wenn onAbbrechen aufgerufen wird', () => {
    const spy = spyOn(router, 'navigate');
    spyOn(store$, 'dispatch');
    fixture.detectChanges();

    component.onAbbrechen();

    expect(store$.dispatch).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();
  });

  // markFormGroupAsTouched
  it('markFormGroupAsTouched', () => {
    const form = new FormGroup({
      control1: new FormControl('', [Validators.required]),
      form2: new FormGroup({
        control2: new FormControl(''),
        array: new FormArray([]) // dieser Fall haben wir noch nicht abgedeckt
      })
    });
    expect(form.controls.control1.touched).toBeFalsy();
    expect((form.controls.form2 as FormGroup).controls.control2.touched).toBeFalsy();

    fixture.componentInstance.markFormGroupAsTouched(form);

    expect(form.controls.control1.touched).toBeTruthy();
    expect((form.controls.form2 as FormGroup).controls.control2.touched).toBeTruthy();
  });

  afterEach(() => {
    fixture.destroy();
  });

});
